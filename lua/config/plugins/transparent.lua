require("transparent").setup({
	-- enable = true,
	extra_groups = {
		"BufferLineTabClose",
		"BufferlineBufferSelected",
		"BufferLineFill",
		"BufferLineBackground",
		"BufferLineSeparator",
		"BufferLineIndicatorSelected",
		"ScrollHead",
		-- "ScrollBody",
		"ScrollTail"
	},
	exclude = {},
})
